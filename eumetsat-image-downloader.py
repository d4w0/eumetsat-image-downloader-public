#!/bin/python3
import logging
import datetime
import json
import sys
import urllib.request
import os
import argparse

from requests import HTTPError

parser = argparse.ArgumentParser(description='Eumetsat Image Downloader')
parser.add_argument("--debug", action="store_true")
parser.add_argument("-V", action="version", version="%(prog)s 0.1")
args = parser.parse_args()

logger = logging.getLogger('Eumetsat Image Downloader')
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
fl = logging.FileHandler("/tmp/eumetsat_image_downloader.log".format())
if args.debug:
    ch.setLevel(logging.DEBUG)
    fl.setLevel(logging.DEBUG)
else:
    ch.setLevel(logging.INFO)
    fl.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
fl.setFormatter(formatter)
logger.addHandler(ch)
logger.addHandler(fl)

# Load config from config file
with open('config.json', 'r') as f:
    config = json.load(f)

base_url = config["main"]["base_url"]
download_base_path = config["main"]["download_base_path"]

rclone_binary = config["rclone"]["binary_path"]
rclone_config_path = config["rclone"]["config_path"]
rclone_move_or_copy = config["rclone"]["move_or_copy"]
rclone_upload_base_path = config["rclone"]["upload_base_path"]
rclone_target = config["rclone"]["upload_target"]


def get_utc_hour_timestamp():
    ts = datetime.datetime.utcnow()
    timestamp = ts.strftime("%Y%m%d-%H")
    return timestamp

def get_utc_year():
    ts = datetime.datetime.utcnow()
    timestamp = ts.strftime("%Y")
    return timestamp

def get_utc_month():
    ts = datetime.datetime.utcnow()
    timestamp = ts.strftime("%m")
    return timestamp

def get_utc_day():
    ts = datetime.datetime.utcnow()
    timestamp = ts.strftime("%d")
    return timestamp


for country in config["images"]:
    directory = download_base_path+country+"/"+get_utc_year()+"/"+get_utc_month()+"/"+get_utc_day()
    if not os.path.exists(directory):
        os.makedirs(directory)
    for image in config["images"][country]:
        download_url=base_url+config["images"][country][image]+".jpg"
        logger.debug("Downloading "+str(download_url))
        try:
            urllib.request.urlretrieve(download_url, directory+"/"+image+"_"+get_utc_hour_timestamp()+".jpg")
        except urllib.error.HTTPError:
            logger.error("Problem while downloading "+str(download_url))


rclone_command = rclone_binary+" --config "+rclone_config_path+" "+rclone_move_or_copy+" "+download_base_path+" "+rclone_target+":"+rclone_upload_base_path
logger.debug("rclone command: "+rclone_command)
os.system(rclone_command)

sys.exit(0)
